import { Component, OnInit } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material';
import { OverlayContainer } from '@angular/cdk/overlay';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'personal-site';
  darkTheme = 'dark-theme';
  lightTheme = 'light-theme';
  theme: string;
  darkThemeIcon = 'fas fa-lightbulb';
  lightThemeIcon = 'far fa-lightbulb';
  themeIcon: string;

  constructor(
    private overlayContainer: OverlayContainer
) {}

  ngOnInit(): void {
    this.theme = this.darkTheme;
    this.themeIcon = this.darkThemeIcon;
  }

  public toggleChange(event: MatSlideToggleChange): void {
    // remove old theme class and add new theme class
    const overlayContainerClasses = this.overlayContainer.getContainerElement().parentElement.classList;
    const themeClassesToRemove = Array.from(overlayContainerClasses).filter(
      (item: string) => item.includes('-theme')
    );
    if (themeClassesToRemove.length) {
       overlayContainerClasses.remove(...themeClassesToRemove);
    }

    if (event.checked) {
      overlayContainerClasses.add(this.darkTheme);
      this.themeIcon = this.darkThemeIcon;
    } else {
      overlayContainerClasses.add(this.lightTheme);
      this.themeIcon = this.lightThemeIcon;
    }
  }
}
