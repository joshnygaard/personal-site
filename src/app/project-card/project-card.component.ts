import { Component, Input } from '@angular/core';

@Component({
  selector: 'project-card',
  templateUrl: './project-card.component.html',
  styleUrls: ['.//project-card.component.scss']
})
export class ProjectCardComponent {
  @Input() cardTitle: string;
  @Input() subtitle: string;
  @Input() imageSrc: string;
  @Input() imageAlt: string;
  @Input() link: string;
  @Input() avatarSrc: string;
}
